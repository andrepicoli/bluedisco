package dominio.cashback;

import java.util.List;
import java.util.UUID;

public interface CashBackService {

	CashBack salvarNovoCashBack(CashBack cashback) throws Exception;
	
	CashBack atualizarCashBack(UUID uuid, CashBack cashBack) throws Exception;
	
	CashBack retornaCashBackPorId(UUID uuid);
	
	CashBack retornaCashBackPorGeneroEDiaDaSemana(String genero_disco, String dia_da_semana);
	
	List<CashBack> retornaTodosCashBacks();
	
	void excluirCashBack(UUID uuid) throws Exception;
	
	void preencherDadosTabelaCashBack() throws Exception;
	
}
