package dominio.cashback;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "cashback")
public class CashBack {

	@Column(name = "id", updatable = false, nullable = false)
	private CashBackId cashback_id;

	@Column(name="genero_disco")
	@NotNull
	private String genero_disco;

	@Column(name="dia_da_semana")
	@NotNull
	private String dia_da_semana;

	@Column(name="percentual")
	@NotNull
	private Integer percentual;

	public CashBack() {

	}

	public CashBack(@NotNull CashBackId cashBackId, @NotNull String genero_disco, @NotNull String dia_da_semana, @NotNull Integer percentual) {
		this.cashback_id = cashback_id;
		this.genero_disco = genero_disco;
		this.dia_da_semana = dia_da_semana;
		this.percentual = percentual;
	}


	public String getGenero_disco() {
		return genero_disco;
	}

	public String getDia_da_semana() {
		return dia_da_semana;
	}

	public CashBackId getCashback_id() {
		return cashback_id;
	}

	public Integer getPercentual() {
		return percentual;
	}

	public void atualizarDadosCashBack(String genero_disco, String dia_da_semana, Integer percentual){
		this.genero_disco = genero_disco;
		this.dia_da_semana = dia_da_semana;
		this.percentual = percentual;
	}

}

