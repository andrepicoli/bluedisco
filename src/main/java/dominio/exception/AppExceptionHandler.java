package dominio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import ports.adapters.controllers.response.Response;
import javassist.NotFoundException;

@ControllerAdvice
public class AppExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(MethodInternalServerErrorException.class)
	public final ResponseEntity<Response> handleAllExceptions(Exception ex, WebRequest request) {
		Response exceptionResponse = new Response(HttpStatus.INTERNAL_SERVER_ERROR.toString(),
				ex.getMessage());
		return new ResponseEntity<>(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public final ResponseEntity<Response> handleMethoArgumentNoValid(NotFoundException ex, WebRequest request) {
		Response exceptionResponse = new Response(HttpStatus.BAD_REQUEST.toString(), ex.getMessage());
		return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
	}
	
}
