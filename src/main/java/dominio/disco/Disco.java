package dominio.disco;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="disco")
public class Disco {

	@Column(name = "id", updatable = false, nullable = false)
	private DiscoId disco_id;

	@Column(name="spotify_id")
	private String spotify_id;
	
	@Column(name="name")
	private String nome_disco;

	@Column(name="price")
	private Double preco;

	@Column(name="valor_cashback")
	private Double valor_cashback;

	@Column(name="genero_disco")
	private String genero_disco;
	
	public Disco(DiscoId disco_id, String spotify_id, String nome_disco, Double preco, Double valor_cashback, String genero_disco) {
		this.disco_id = disco_id;
		this.spotify_id = spotify_id;
		this.nome_disco = nome_disco;
		this.preco = preco;
		this.valor_cashback = valor_cashback;
		this.genero_disco = genero_disco;
	}


}
