package dominio.disco;

import java.util.List;

public interface DiscoService {

	List<Disco> findAllDiscosByGenre(String genre) throws Exception;
	
	Disco findDiscoById(String id)  throws Exception;
	
}
