package dominio.venda;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface VendaService {

	List<Venda> getAllSales();
	
	Venda findSaleByUuid(UUID uuid);
	
	List<Venda> findAllSaleFilterByDate(Date initialDate, Date finalDate);
	
	Venda saveASale(Venda sale);
	
	Venda updateASale(UUID uuid, Venda sale);
	
	void deleteASale(UUID uuid) throws Exception;
	
}
