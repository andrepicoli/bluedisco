package dominio.venda;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import dominio.disco.Disco;
import dominio.disco.DiscoId;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="sale")
public class Venda {
	
	@Column(name = "venda_id", updatable = false, nullable = false)
	private VendaId venda_id;

	@Column(name="nome_cliente")
	private String nome_cliente;

	@Column(name="valor_total_cashback")
	private Double valor_total_cashback;
	
	@Column(name="data_criacao")
	@Temporal(TemporalType.DATE)
    Date data_criacao;

	 @OneToMany(
		        cascade = CascadeType.ALL,
		        orphanRemoval = true
		    )
    private List<DiscoId> discos = new ArrayList<>();

	public Venda(VendaId venda_id, String nome_cliente, Double valor_total_cashback, Date data_criacao, List<DiscoId> discos) {
		this.venda_id = venda_id;
		this.nome_cliente = nome_cliente;
		this.valor_total_cashback = valor_total_cashback;
		this.data_criacao = data_criacao;
		this.discos = discos;
	}
}
