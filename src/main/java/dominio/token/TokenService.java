package dominio.token;

public interface TokenService {
	Token generateToken() throws Exception;
}
