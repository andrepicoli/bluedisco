package ports.adapters.controllers.sale;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dominio.venda.Venda;
import javassist.tools.web.BadHttpRequest;
import dominio.venda.VendaService;

@RestController
@RequestMapping("/sale")
public class SaleController {

	@Autowired
	VendaService service;
	
	@GetMapping()
	private ResponseEntity<List<Venda>> getAllSales(){
		return ResponseEntity.ok(service.getAllSales());
	}
	
	@GetMapping("/{id}")
	private ResponseEntity<Venda> findSaleByUuid(@PathVariable("id") String id){
		return ResponseEntity.ok(service.findSaleByUuid(UUID.fromString(id)));
	}
	
    @GetMapping("/filter")
    private ResponseEntity<List<Venda>> getAllSales(Pageable pageable, @RequestParam(required=true) String initialDate, @RequestParam(required=true) String finalDate) throws ParseException {
    	Date init = new SimpleDateFormat("yyyy-MM-dd").parse(initialDate);
    	Date end = new SimpleDateFormat("yyyy-MM-dd").parse(finalDate);
    	return ResponseEntity.ok(service.findAllSaleFilterByDate(init, end));
    }
    
    @PostMapping()
    private ResponseEntity<Venda> saleDiscos(@Valid @RequestBody Venda sale, Pageable pageable) throws BadHttpRequest, ClientProtocolException, IOException {
    	return ResponseEntity.ok(service.saveASale(sale));
    }
    
    @PutMapping("/{id}")
    private ResponseEntity<Venda> updateSale(@PathVariable("id") String id, @RequestBody Venda sale, Pageable pageable) throws BadHttpRequest, ClientProtocolException, IOException {
    	return ResponseEntity.ok(service.updateASale(UUID.fromString(id), sale));
    }
    
    @DeleteMapping("/{id}")
    private ResponseEntity<String> deleteSale(@PathVariable("id") String id) throws Exception{
    	service.deleteASale(UUID.fromString(id));
    	return ResponseEntity.ok("Sale id '"+id+"' was deleted with success!");
    }
	
}
