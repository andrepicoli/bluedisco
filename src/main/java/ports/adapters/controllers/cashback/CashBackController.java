package ports.adapters.controllers.cashback;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dominio.cashback.CashBack;
import dominio.cashback.CashBackService;

@RestController
@RequestMapping("/cashback")
public class CashBackController {

	@Autowired
	CashBackService service;

	@PostMapping
	private ResponseEntity<CashBack> salvarCashBackDetalhes(@Valid @RequestBody(required=true) CashBack cashback) throws Exception {
		return ResponseEntity.ok(service.save(cashback));
	}
	
	@PutMapping("/{id}")
	private ResponseEntity<CashBack> atualizarCashBackDetalhes(@PathVariable("id") String id, @RequestBody(required=true) CashBack cashBack) throws Exception{
		return ResponseEntity.ok(service.update(UUID.fromString(id), cashBack));
	}

	@GetMapping("/{id}")
	private ResponseEntity<CashBack> retornaCashBackPorId(@PathVariable("id") String id){
		return ResponseEntity.ok(service.findOneByUuid(UUID.fromString(id)));
	}

	@GetMapping("/filter")
	private ResponseEntity<CashBack> retornaCashBackPorGeneroEDiaDaSemana(@RequestParam(required=true) String genre, @RequestParam(required=true) String weekDay){
		return ResponseEntity.ok(service.findCashBackByGenreAndWeekday(genre, weekDay));
	}

	@GetMapping()
	private ResponseEntity<List<CashBack>> retornaTodosCashBacks(){
		return ResponseEntity.ok(service.findAllCashBacksAvailables());
	}

	@DeleteMapping("/{id}")
	private ResponseEntity<String> deleteCashBack(@PathVariable("id") String id) throws Exception {
		service.deleteCashBack(UUID.fromString(id)); 
		return ResponseEntity.ok("CashBack id '"+id+"' was deleted with success!");
	}

	@GetMapping("/fill")
	private ResponseEntity<String> fillDataIntoDB() throws Exception {
		service.fillDataIntoDB();
		return ResponseEntity.ok("CashBack datas was filled with success!");
	}
	
}
