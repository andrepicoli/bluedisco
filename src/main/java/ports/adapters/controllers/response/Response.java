package ports.adapters.controllers.response;

import java.util.Date;

public class Response {

	private Date timestamp;
	private String status;
	private String mensagem;
	
	public Response(String status, String mensagem) {
		this.timestamp = new Date();
		this.status = status;
		this.mensagem = mensagem;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public String getStatus() {
		return status;
	}

	public String getMensagem() {
		return mensagem;
	}
	
}
