package ports.adapters.controllers.disco;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dominio.disco.Disco;
import dominio.disco.DiscoService;

@RestController
@RequestMapping("/disco")
public class DiscoController {

	@Autowired DiscoService service;
	
	@GetMapping()
	private ResponseEntity<List<Disco>> findAllDiscosByGenre(@RequestParam(required=true) String genre) throws Exception {
		return ResponseEntity.ok(service.findAllDiscosByGenre(genre));
	}

	@GetMapping("/{id}")
	private ResponseEntity<Disco> findADiscoByIdSpotify(@PathVariable("id") String id) throws Exception {
		return ResponseEntity.ok(service.findDiscoById(id));
	}
	
}
