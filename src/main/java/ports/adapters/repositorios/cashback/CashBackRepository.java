package ports.adapters.repositorios.cashback;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import dominio.cashback.CashBack;

@Repository
public interface CashBackRepository extends JpaRepository<CashBack, String> {

	CashBack findOneByUuid(UUID uuid);

	@Query("select c from CashBack c where c.genre=?1 and c.weekDay=?2")
	CashBack findByWeekDayAndGenre(String genre, String weekDay);

	@Transactional
	@Modifying
	@Query("delete from CashBack c where c.uuid = :id")
	void deleteCashBack(@Param("id") UUID uuid);

}
