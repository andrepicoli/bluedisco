package ports.adapters.repositorios.cashback;

import java.util.List;
import java.util.UUID;

import dominio.cashback.CashBack;
import dominio.cashback.CashBackId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dominio.exception.MethodArgumentNotValidException;

import dominio.cashback.CashBackService;

@Service
public class CashBackImpl implements CashBackService{

	@Autowired
	CashBackRepository repository;
	
	@Override
	public CashBack salvarNovoCashBack(CashBack cashback) throws Exception {
		CashBack cashBack = repository.findByWeekDayAndGenre(cashback.getGenero_disco(), cashback.getDia_da_semana());
		
		if(cashBack != null) {
			throw new MethodArgumentNotValidException("CashBack with id '"+cashback.getCashback_id().getCashback_id()+"' already exists!");
		}
		
		return repository.save(cashback);
	}

	@Override
	public CashBack atualizarCashBack(UUID uuid, CashBack cashBack) throws Exception {
		CashBack cash = repository.findOneByUuid(uuid);

		if(cash == null) {
			throw new MethodArgumentNotValidException("CashBack with id: '"+uuid+"' does not exists!");
		}

		cash.atualizarDadosCashBack(cashBack.getGenero_disco(), cashBack.getDia_da_semana(), cashBack.getPercentual());
		return repository.save(cash);
	}

	@Override
	public CashBack retornaCashBackPorId(UUID uuid) {
		CashBack cashBack = repository.findOneByUuid(uuid);
		
		if(cashBack == null) {
			throw new MethodArgumentNotValidException("CashBack with id '"+uuid+"' does not exists!");
		}
		
		return repository.findOneByUuid(uuid);
	}

	@Override
	public CashBack retornaCashBackPorGeneroEDiaDaSemana(String genre, String weekDay) {
		
		CashBack cashBack = repository.findByWeekDayAndGenre(genre.toUpperCase(), weekDay);
		
		if(cashBack == null) {
			throw new MethodArgumentNotValidException("CashBack with Genre '"+genre+"' and WeekDay "+weekDay+" was not found!");
		}
		
		return cashBack;
	}

	@Override
	public List<CashBack> retornaTodosCashBacks() {
		return repository.findAll();
	}

	@Override
	public void excluirCashBack(UUID uuid) throws Exception {
		CashBack cashback = repository.findOneByUuid(uuid);

		if(cashback == null) {
			throw new MethodArgumentNotValidException("CashBack with uuid '"+uuid+"' does not exists!");
		}
		
		repository.deleteCashBack(cashback.getCashback_id().getCashback_id());
	}

	@Override
	public void preencherDadosTabelaCashBack() throws Exception {

		repository.save(new CashBack(new CashBackId(),"POP", "SUNDAY", 25));
		repository.save(new CashBack(new CashBackId(),"POP", "MONDAY", 7));
		repository.save(new CashBack(new CashBackId(),"POP", "TUESDAY", 7));
		repository.save(new CashBack(new CashBackId(),"POP", "WEDNESDAY", 2));
		repository.save(new CashBack(new CashBackId(),"POP", "THURSDAY", 10));
		repository.save(new CashBack(new CashBackId(),"POP", "FRIDAY", 15));
		repository.save(new CashBack(new CashBackId(),"POP", "SATURDAY", 20));
		
		repository.save(new CashBack(new CashBackId(),"MPB", "SUNDAY", 30));
		repository.save(new CashBack(new CashBackId(),"MPB", "MONDAY", 5));
		repository.save(new CashBack(new CashBackId(),"MPB", "TUESDAY", 10));
		repository.save(new CashBack(new CashBackId(),"MPB", "WEDNESDAY", 15));
		repository.save(new CashBack(new CashBackId(),"MPB", "THURSDAY", 20));
		repository.save(new CashBack(new CashBackId(),"MPB", "FRIDAY", 25));
		repository.save(new CashBack(new CashBackId(),"MPB", "SATURDAY", 30));
		
		repository.save(new CashBack(new CashBackId(),"ROCK", "SUNDAY", 40));
		repository.save(new CashBack(new CashBackId(),"ROCK", "MONDAY", 10));
		repository.save(new CashBack(new CashBackId(),"ROCK", "TUESDAY", 15));
		repository.save(new CashBack(new CashBackId(),"ROCK", "WEDNESDAY", 15));
		repository.save(new CashBack(new CashBackId(),"ROCK", "THURSDAY", 15));
		repository.save(new CashBack(new CashBackId(),"ROCK", "FRIDAY", 20));
		repository.save(new CashBack(new CashBackId(),"ROCK", "SATURDAY", 40));
		
		repository.save(new CashBack(new CashBackId(),"CLASSIC", "SUNDAY", 35));
		repository.save(new CashBack(new CashBackId(),"CLASSIC", "MONDAY", 3));
		repository.save(new CashBack(new CashBackId(),"CLASSIC", "TUESDAY", 5));
		repository.save(new CashBack(new CashBackId(),"CLASSIC", "WEDNESDAY", 8));
		repository.save(new CashBack(new CashBackId(),"CLASSIC", "THURSDAY", 13));
		repository.save(new CashBack(new CashBackId(),"CLASSIC", "FRIDAY", 18));
		repository.save(new CashBack(new CashBackId(),"CLASSIC", "SATURDAY", 25));

	}

}
