package ports.adapters.repositorios.token;

import java.util.Base64;

import dominio.token.Token;
import dominio.token.TokenService;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Service
public class TokenImpl implements TokenService {

	@Override
	public Token generateToken() throws Exception {
		
		String CLIENT_ID = "bfa9140dc069452ba8057ebf70b58a7c";
		String CLIENT_SECRET = "057ef8cb5f2648cda6bc2f3a60f60d7d";

		String responseString = "";
		String authorization = CLIENT_ID+":"+CLIENT_SECRET;

		authorization = "Basic "+Base64.getEncoder().encodeToString(authorization.getBytes());

		try {

			HttpClient client = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost("https://accounts.spotify.com/api/token");

			String param = "grant_type=client_credentials";
			StringEntity entity = new StringEntity(param);
			httpPost.setEntity(entity);
			httpPost.setHeader("Authorization", authorization);
			httpPost.setHeader("Content-type", "application/x-www-form-urlencoded");

			HttpResponse response = client.execute(httpPost);
			responseString = new BasicResponseHandler().handleResponse(response);

			Gson gson = new Gson();
			return gson.fromJson(responseString, new TypeToken<Token>(){}.getType());

		}catch(Exception e) {
			throw new Exception("Não foi possível gerar o token!");
		}
	}
}
