package ports.adapters.repositorios.disco;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import dominio.cashback.CashBack;
import dominio.cashback.CashBackService;
import dominio.disco.Disco;
import dominio.disco.DiscoService;
import dominio.token.Token;
import dominio.token.TokenService;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dominio.exception.MethodArgumentNotValidException;
import dominio.exception.MethodInternalServerErrorException;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Service
public class DiscoImpl implements DiscoService {

	@Autowired
	private TokenService tokenService;

	@Autowired
	private CashBackService cashBackService;

	private Token token;

	private static String MARKET = "market=BR";
	private static HttpClient client = HttpClients.createDefault();
	private static HttpGet httpget;
	private static List<Disco> discos = new ArrayList<Disco>();
	private static Disco disco;
	private static JsonArray jsonArray;

	@Override
	public List<Disco> findAllDiscosByGenre(String genre) throws Exception {

		token = tokenService.generateToken();

		if(token == null) {
			throw new MethodInternalServerErrorException("Não foi possível gerar o Token! Problemas com a API do Spotify!");
		}

		HttpGet httpget;
		String responseString;
		JsonElement jsonobj;
		HttpResponse response;
		List<String> albums = new ArrayList<String>();

		httpget = new HttpGet("https://api.spotify.com/v1/search?q=genre:"+genre+"&type=track&limit=50&"+MARKET);

		httpget.addHeader("Authorization",token.getToken_type()+" "+token.getAccess_token());
		httpget.addHeader("Content-Type","application/json");

		responseString = "";

		jsonobj = null;

		response = client.execute(httpget);
		responseString = new BasicResponseHandler().handleResponse(response);

		JsonParser parser = new JsonParser(); 
		jsonobj = (JsonObject) parser.parse(responseString);
		jsonobj = ((JsonObject) jsonobj).get("tracks");
		jsonobj = ((JsonObject) jsonobj).get("items");

		//Creating jsonArray to loop and get tracks' albums
		jsonArray = jsonobj.getAsJsonArray();

		for (JsonElement jsonElement : jsonArray) {
			if(!jsonElement.isJsonNull()) {
				JsonElement jElement = null;
				jElement = jsonElement.getAsJsonObject().get("album");
				String album = jElement.getAsJsonObject().get("href").toString();
				album = album.replace("\"","");
				albums.add(album);
			}
		}

		parser = null;
		responseString = null;

		discos = new ArrayList<Disco>();

		for (String url : albums) {

			url = url+"?"+MARKET;
			httpget = new HttpGet(url);

			httpget.addHeader("Authorization",token.getToken_type()+" "+token.getAccess_token());
			httpget.addHeader("Content-Type","application/json");

			response = client.execute(httpget);
			responseString = new BasicResponseHandler().handleResponse(response);

			parser = new JsonParser(); 
			jsonobj = (JsonObject) parser.parse(responseString);

			double price = (double) Math.round(Math.random()*10);

			//Finding percentual of genre and current day
			Calendar calendar = Calendar.getInstance();
			int day = calendar.get(Calendar.DAY_OF_WEEK); 

			String toDay = DayOfWeek.of(day).toString();

			CashBack cashbackObj = cashBackService.findCashBackByGenreAndWeekday(genre, toDay);

			if(cashbackObj == null) {
				throw new MethodArgumentNotValidException("CashBack with Genre '"+genre+"' and WeekDay "+toDay+" was not found!");
			}

			Integer percent = cashbackObj.getPercent();

			double cashback = ((price * percent)/100);

			String id_spotify = jsonobj.getAsJsonObject().get("id").getAsString();

			String name = jsonobj.getAsJsonObject().get("name").getAsString();

			disco = new Disco(id_spotify, 
					name, 
					price,
					cashback,
					genre
					);

			//Adding Discos inside of an Array
			discos.add(disco);
			disco = null;
		}	
		return discos;
	}

	@Override
	public Disco findDiscoById(String id) throws Exception {

		String responseString = null;
		HttpResponse response;
		JsonParser parser = new JsonParser();
		JsonElement jsonobj;


		this.token = tokenService.generateToken();

		if(token == null) {
			throw new MethodInternalServerErrorException("Não foi possível gerar o Token! Problemas com a API do Spotify!");
		}

		httpget = new HttpGet("https://api.spotify.com/v1/albums/"+id+"?"+MARKET);

		httpget.addHeader("Authorization",this.token.getToken_type()+" "+token.getAccess_token());
		httpget.addHeader("Content-Type","application/json");

		response = client.execute(httpget);
		
		if(response == null) {
			throw new MethodArgumentNotValidException("Disco with id '"+id+"' was not found!");
		}
		
		responseString = new BasicResponseHandler().handleResponse(response);

		parser = new JsonParser(); 
		jsonobj = (JsonObject) parser.parse(responseString);

		//Constructor to new Object
		String spotify_id = jsonobj.getAsJsonObject().get("id").getAsString();
		String name = jsonobj.getAsJsonObject().get("name").getAsString();
		double price = (double) Math.round(Math.random()*10);

		jsonobj = (JsonObject) parser.parse(responseString);
		jsonobj = ((JsonObject) jsonobj).get("artists");

		JsonArray jsonArray = jsonobj.getAsJsonArray();

		String url = jsonArray.get(0).getAsJsonObject().get("href").getAsString();

		httpget = new HttpGet(url+"?"+MARKET);

		httpget.addHeader("Authorization",token.getToken_type()+" "+token.getAccess_token());
		httpget.addHeader("Content-Type","application/json");

		try {
			response = client.execute(httpget);
			responseString = new BasicResponseHandler().handleResponse(response);
		} catch (Exception e) {
			e.printStackTrace();
		}

		jsonobj = (JsonObject) parser.parse(responseString);
		jsonobj = ((JsonObject) jsonobj).get("genres");

		JsonArray jRay = new JsonArray();

		jRay = jsonobj.getAsJsonArray();

		String genre = "";

		/**
		 * It was necessary cause sometimes genre comes as 'pop rock'...
		 * and I need to get the correct in cashback table the right value
		 */

		for (JsonElement jsonElement : jRay) {
			if(jsonElement.getAsString().contains("pop")) {
				genre = "pop";
				break;
			}else if(jsonElement.getAsString().contains("rock")) {
				genre = "rock";
				break;
			}else if(jsonElement.getAsString().contains("mpb")) {
				genre = "mpb";
				break;
			}else if(jsonElement.getAsString().contains("classic")) {
				genre = "classic";
				break;
			}
		}

		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK); 

		String toDay = DayOfWeek.of(day).toString();

		CashBack cashbackObj = cashBackService.findCashBackByGenreAndWeekday(genre, toDay);

		if(cashbackObj == null) {
			throw new MethodArgumentNotValidException("CashBack with Genre '"+genre+"' and WeekDay "+toDay+" was not found!");
		}

		Integer percent = cashbackObj.getPercent();

		double cashback = (price * percent)/100;

		disco = new Disco(spotify_id, name, price, cashback, genre);

		return disco;

	}

}

