package ports.adapters.repositorios.sale;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import dominio.venda.Venda;

@Repository
public interface SaleRepository extends JpaRepository<Venda, String> {
	
	Venda findOneByUuid(UUID uuid);
	
	@Query("select s from Sale s where creation_date >= :initialDate and creation_date <= :finalDate order by creation_date")
	List<Venda> findAllWithCreationDate(@Param("initialDate") Date initialDate, @Param("finalDate") Date finalDate);

	@Transactional
	@Modifying
	@Query("delete from Sale s where s.uuid = :id")
	void deleteCashBackByUuid(@Param("id") UUID uuid);
	
}
