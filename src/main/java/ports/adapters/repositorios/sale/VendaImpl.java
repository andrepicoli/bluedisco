package ports.adapters.repositorios.sale;

import java.time.DayOfWeek;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import dominio.cashback.CashBack;
import dominio.disco.Disco;
import dominio.venda.Venda;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dominio.exception.MethodArgumentNotValidException;

import dominio.cashback.CashBackService;
import dominio.venda.VendaService;

@Service
public class VendaImpl implements VendaService {

	@Autowired
	SaleRepository repository;

	@Autowired
	CashBackService cashBackService;

	@Override
	public List<Venda> getAllSales() {
		return repository.findAll();
	}

	@Override
	public Venda findSaleByUuid(UUID uuid) {
		Venda sale = repository.findOneByUuid(uuid);
		if(sale == null) {
			throw new MethodArgumentNotValidException("Sale with id '"+uuid+"' does not exists!");
		}
		return sale;
	}

	@Override
	public List<Venda> findAllSaleFilterByDate(Date initialDate, Date finalDate) {
		return repository.findAllWithCreationDate(initialDate, finalDate);
	}

	@Override
	public Venda saveASale(Venda sale) {

		List<Disco> discos = sale.getDiscos();

		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK); 

		String toDay = DayOfWeek.of(day).toString();
		Double totalCashBack = 0.00;

		for (Disco disco : discos) {
			CashBack cash = cashBackService.findCashBackByGenreAndWeekday(disco.getGenre(), toDay);
			double price = disco.getPrice() == null ? (double) Math.round(Math.random()*10) : disco.getPrice();
			Double cashback = (price * cash.getPercent())/100;
			totalCashBack += cashback;
		}
		
		sale.fillDateAndCashBackTotal(totalCashBack);
		return repository.save(sale);
	}

	@Override
	public Venda updateASale(UUID uuid, Venda sale) {
		Venda exists = repository.findOneByUuid(uuid);
		
		if(exists == null) {
			throw new MethodArgumentNotValidException("Sale with id: '"+uuid+"' does not exists!");
		}
		
		exists.updateSaleData(sale.getCostumerName(), sale.getDiscos());
		
		return repository.save(exists);
	}

	@Override
	public void deleteASale(UUID uuid) throws Exception {
	Venda exists = repository.findOneByUuid(uuid);
		
		if(exists == null) {
			throw new MethodArgumentNotValidException("Sale with id: '"+uuid+"' does not exists!");
		}
		
		repository.deleteCashBackByUuid(uuid);
		
	}

}
