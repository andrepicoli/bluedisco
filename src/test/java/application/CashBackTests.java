package application;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import ports.adapters.controllers.cashback.CashBackController;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import dominio.cashback.CashBack;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import io.restassured.module.mockmvc.response.MockMvcResponse;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CashBackTests {

	@Autowired
	CashBackController controller;

	@LocalServerPort
	private int port;
	
	static CashBack generated;

	@Test()
	public void stage_A_shouldInsertACashBack() throws JsonParseException, JsonMappingException, IOException {

		String json = new String(Files.readAllBytes(Paths.get("src/test/java/com/bluedisco/mock/CashBackMockupInsert.json")), StandardCharsets.UTF_8);

		MockMvcResponse response = RestAssuredMockMvc.
		given().
		standaloneSetup(controller).
		contentType(ContentType.JSON).
		body(json).
		post("/bluedisco/cashback").
		then().
		statusCode(200)
		.extract()
		.response();
		
		Gson g = new Gson();
		generated = g.fromJson(response.getBody().asString(), CashBack.class);

	}
	
	@Test()
	public void stage_B_shouldUpdateACashBack() throws JsonParseException, JsonMappingException, IOException {

		String json = new String(Files.readAllBytes(Paths.get("src/test/java/com/bluedisco/mock/CashBackMockupUpdate.json")), StandardCharsets.UTF_8);

		RestAssured.
		given().
		port(port).
		contentType(ContentType.JSON).
		body(json).
		pathParam("id",generated.getUuid()).
		put("/bluedisco/cashback/{id}").
		then().
		statusCode(200);

	}

	@Test()
	public void stage_C_shoulGetAllAvailablesCashBacks() {
		RestAssuredMockMvc.
		given().
		standaloneSetup(controller).
		contentType(ContentType.JSON).
		get("/bluedisco/cashback").
		then().
		statusCode(200);
	}		
	
	@Test()
	public void stage_D_shoulGetACashBackByUuid() {
		
		RestAssured.
		given().
		port(port).
		contentType(ContentType.JSON).
		pathParam("id",generated.getUuid()).
		get("/bluedisco/cashback/{id}").
		then().
		statusCode(200);
	}		

	@Test()
	public void stage_E_shoulGetACashBackByGenreAndWeekDay() throws IOException {

		String json = new String(Files.readAllBytes(Paths.get("src/test/java/com/bluedisco/mock/CashBackMockupUpdate.json")), StandardCharsets.UTF_8);

		Gson g = new Gson();
		CashBack cash = g.fromJson(json, CashBack.class);

		RestAssured.
		given().
		port(port).
		contentType(ContentType.JSON).
		queryParam("genre",cash.getGenre()).
		queryParam("weekDay",cash.getWeekDay()).
		get("/bluedisco/cashback/filter").
		then().
		statusCode(200);
	}


	@Test
	public void stage_F_shoulDeleteACashBackByGenreAndWeekDay() throws IOException  {

		RestAssured.
		given().
		port(port).
		contentType(ContentType.JSON).
		pathParam("id",generated.getUuid()).
		delete("/bluedisco/cashback/{id}").
		then().
		statusCode(200);

	}
	
}

