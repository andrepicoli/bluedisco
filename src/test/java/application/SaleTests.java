package application;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.gson.Gson;

import dominio.venda.Venda;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SaleTests {
	
	@LocalServerPort
	private int port;
	
	static Venda saleGenerated;
	
	@Test()
	public void stage_A_shouldInsertASale() throws IOException {
		
		String json = new String(Files.readAllBytes(Paths.get("src/test/java/com/bluedisco/mock/SaleMackInsert.json")), StandardCharsets.UTF_8);

		Response response = RestAssured.
		given().
		port(port).
		contentType(ContentType.JSON).
		body(json).
		post("/bluedisco/sale").
		then().
		statusCode(200).
		extract().
		response();

		Gson gson = new Gson();
		saleGenerated = gson.fromJson(response.getBody().asString(), Venda.class);
		
	}
	
	@Test()
	public void stage_B_shouldGetASaleByUuid() {
		
		RestAssured.
		given().
		port(port).
		contentType(ContentType.JSON).
		pathParam("id", saleGenerated.getUuid()).
		get("/bluedisco/sale/{id}").
		then().
		statusCode(200);
		
	}
	
	@Test()
	public void stage_C_shouldGetASaleByInitialDateAndFinalDate() throws ParseException {
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
    	String init = format.format( new Date());
    	String end = format.format( new Date());
		
		RestAssured.
		given().
		port(port).
		contentType(ContentType.JSON).
		queryParam("initialDate", init).
		queryParam("finalDate", end).
		get("/bluedisco/sale/filter").
		then().
		statusCode(200);
		
	}
	
	@Test()
	public void stage_D_shouldGetAllSales() {
		
		RestAssured.
		given().
		port(port).
		contentType(ContentType.JSON).
		get("/bluedisco/sale").
		then().
		statusCode(200);
		
	}
	
	@Test()
	public void stage_E_shouldUpdateASale() throws IOException {
		
		String json = new String(Files.readAllBytes(Paths.get("src/test/java/com/bluedisco/mock/SaleMackUpdate.json")), StandardCharsets.UTF_8);
		
		RestAssured.
		given().
		port(port).
		contentType(ContentType.JSON).
		pathParam("id", saleGenerated.getUuid()).
		body(json).
		put("/bluedisco/sale/{id}").
		then().
		statusCode(200);
		
	}
	
	@Test()
	public void stage_F_shouldDeleteASale() throws IOException {
		
		RestAssured.
		given().
		port(port).
		contentType(ContentType.JSON).
		pathParam("id", saleGenerated.getUuid()).
		delete("/bluedisco/sale/{id}").
		then().
		statusCode(200);
		
	}
	
}
