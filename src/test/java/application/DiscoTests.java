package application;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.gson.Gson;

import dominio.disco.Disco;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DiscoTests {
	
	@LocalServerPort
	private int port;
	
	@Test()
	public void stage_A_shouldgetManyDiscosByGenre() throws IOException {
		
		String json = new String(Files.readAllBytes(Paths.get("src/test/java/com/bluedisco/mock/DiscoMock.json")), StandardCharsets.UTF_8);

		Gson gson = new Gson();
		Disco disco = gson.fromJson(json, Disco.class);
		
		RestAssured.
		given().
		port(port).
		contentType(ContentType.JSON).
		queryParam("genre",disco.getGenre()).
		get("/bluedisco/disco").
		then().
		statusCode(200);
		
	}
	
	@Test()
	public void stage_B_shouldgetManyDiscosByIdSpotify() throws IOException {
		
		String json = new String(Files.readAllBytes(Paths.get("src/test/java/com/bluedisco/mock/DiscoMock.json")), StandardCharsets.UTF_8);

		Gson gson = new Gson();
		Disco disco = gson.fromJson(json, Disco.class);
		
		RestAssured.
		given().
		port(port).
		contentType(ContentType.JSON).
		pathParam("id",disco.getSpotify_id()).
		get("/bluedisco/disco/{id}").
		then().
		statusCode(200);
		
	}

}
