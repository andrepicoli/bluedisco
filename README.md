#bluedisco API

Starting to clone project...

**git clone git@bitbucket.org:andrepicoli/bluedisco.git**

Now, lets install all dependencies and run tests

**mvn clean install**

**mvn eclipse\:eclipse**

**mvn test**

Next step, we will create the database

To make the Postgres configuration easy, I used a docker Postgres to create my database.
Run the following code to start database.

**sudo docker run --name "postgis" -p 25432:5432 -d -t kartoza/postgis**

After that, create a new schema called 'vdisco' to Hibernate create all tables and relations.

To fill cashback table I created a simple call in Rest with Mock Data.

**http://localhost:8080/bluedisco/cashback/fill**

Do that and all CashBack data will be in database...

After all of this steps, feel free to use the API.

Thanks for the opportunity!